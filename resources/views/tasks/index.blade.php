

@extends('layouts.app')
@section('content')

<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=3, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<div class='container'>
    <br/><br/>
    <h1>Task list</h1>
@if (Request::is('tasks'))  

<h5><a href="{{action('TaskController@mytasks')}}">My Tasks</a></h5>
@else 
<h5><a href="{{action('TaskController@index')}}">All Tasks</a></h5>

@endif

<table class="table">
<tr>
    <th>Task</th>
    <th>Edit </th>
    @can('admin') <th>Mark as Done</th>
    <th>Delete</th>@endcan
</tr>



<tr>
@foreach($tasks as $task)
<td> {{$task->title}}</td>

<td> <a href = "{{route('tasks.edit',$task->id)}}">  Edit </a></td>
<td>

@can('admin')
    @if ($task->status==1)
           <h4>Done!</h4>
           <!-- <button id ="{{$task->id}}" value="1"> Done!</button> add option to change the status back-->
       @else
       <a href="{{route('done', $task->id)}}">Mark as Done</a>
       @endif</td>

</td>
<td>
<a href="{{route('delete', $task->id)}}">Delete</a>
</td>
@endcan
</tr>





@endforeach

</table>
<br><br>
<a href = "{{route('tasks.create')}}">Add a new Task </a>

<script>
        $(document).ready(function(){
            
            $("button").click(function(event){
                console.log('event.target.id')
                $.ajax({
                   url:  "{{url('tasks')}}" + '/' + event.target.id,
                   dataType:  'json',
                   type:  'put' ,
                    contentType: 'application/json',
                   data: JSON.stringify({'status':event.target.value=1, _token:'{{csrf_token()}}'}),
                    processData: false,
                    success: function( data){
                         console.log(JSON.stringify( data ));
                    },
                    error: function(errorThrown ){
                        console.log( errorThrown );
                    }
                });   
                location.reload();
            
            });
        });

        </div>
    </script> 








   @endsection